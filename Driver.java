import java.util.Scanner;

public class Driver {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Hello, user"); // greets user
		
		System.out.println("Please enter 2 numbers to add together"); // asks the user to enter 2 numbers
		
		System.out.println("Number 1");
		int num1 = sc.nextInt();
		
		System.out.println("Number 2");
		int num2 = sc.nextInt();
		
		int sum = Calculator.add(num1, num2); // calculates sum
		System.out.println("The sum = " + sum);
		
		System.out.println("Please enter a number to find the square root of"); // asks the user for a number to square root
		
		System.out.println("Enter number");
		int num3 = sc.nextInt();
		
		double squareRoot = Calculator.squareRoot(num3); // calculates the squareRoot
		System.out.println("The square root of " + num3 + " = " + squareRoot);

		int randomNum = Calculator.random(); // creates a random number
		System.out.println("Here is a random num :) " + randomNum);
		
	}
	
}