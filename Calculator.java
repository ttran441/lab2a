import java.util.Random;

public class Calculator {
	
	// returns the sum of two numbers
	public static int add(int num1, int num2) {
		
		return num1 + num2;
		
	}
	
	// returns the square root of a number
	public static double squareRoot(int num) {
		
		return Math.sqrt(num);
		
	}
	
	// returns a random number from 1 to 99
	public static int random() {
		
		Random random = new Random();
		final int MAX = 99;
		
		return random.nextInt(MAX) + 1;
		
	}
	
	// returns the quotient of num1 divided by num2
	public static double divide(int num1, int num2) {
		
		if (num1 == 0) {
			
			return 0;
			
		}
		
		else if (num2 == 0) {
			
			return -1;
			
		}
		
		return num1/num2;
		
	}
	
}